## RxjsFlux - React state management using RxJS

A POC on using RxJS to manage application state for a React application.  The ideas are greatly influenced by Redux though.  I think synchronous update is already working so the next experiment is how to deal with async operations.  Also, I really like Redux's middleware concept so that is also another great feature to add.
