import { DataArea } from "./DataArea";
import { withStateContext } from "./rxredux";
import { getSomeData, someDataReceived, syncPushData } from "./actions";

const stateSelector = ({ someList }) => ({ someList }); // a selector, like mapStateToProps
const actions = { getSomeData, someDataReceived, syncPushData };

export const ConnectedDataArea = withStateContext (
	 stateSelector,
	 actions
)(DataArea);
