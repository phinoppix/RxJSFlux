import React, { PureComponent } from 'react';

import { createStore, Provider } from "./rxredux";
import { Workspace } from "./Workspace";

const reducers = {
	someList: (state = {}, action) => {
		switch(action.type) {
			case "SYNC_PUSH_DATA":
				return {
					...state,
					counter: action.payload.counter
				};
			default:
				return state;
		}
	},
	others: (state = {}, action) => { return state; },
};

const middlewares = [
	function(action, prevState, nextState) {
		console.group("Action", action);
		console.log("prev", prevState);
		console.log("next", nextState);
		console.groupEnd();
	}
];

const store = createStore(reducers, middlewares);

export class MyApp extends PureComponent {
	render() {
		return (
			<Provider store={store}>
				<Workspace />
			</Provider>
		);
	}
}
