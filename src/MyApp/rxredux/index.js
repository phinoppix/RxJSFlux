import React, { PureComponent, createElement } from "react";
import { BehaviorSubject, Subject } from "rxjs";
import { toPairs } from "ramda";

class Store {
	action$ = undefined;
	store$ = undefined;
	middleWares = [];

	constructor(reducers, middleWares = []) {
		const reducersPair = toPairs(reducers);
		const initState = reducersPair.reduce((acc, cur) => ({
			...acc,
			[cur[0]]: cur[1](undefined, Object.prototype)
		}), {});

		this.action$ = new Subject(); // TODO: Observer
		this.store$ = new BehaviorSubject(initState);

		this.action$.subscribe(action => {
			if (!action) return;
			const state = this.store$.value;
			const newState = reducersPair.reduce((acc, reducer) => {
				const [name, fn] = reducer;
				return {
					...acc,
					[name]: fn(state[name], action)
				}
			}, state);
			if (state !== newState)
				this.store$.next(newState);
			// TODO: Middlewares not properly implemented yet.
			middleWares.map(w => w(action, state, newState));
		});
	}

	getState() {
		return this.store$.value;
	}

	next(value) {
		this.action$.next(value);
	}
}

export const createStore = (reducers, middleWares) => {
	return new Store(reducers, middleWares);
};


export var StoreContext = React.createContext({});

export class Provider extends PureComponent {
	state = {
		store: undefined,
		data: undefined,
	};

	componentDidMount() {
		const { store } = this.props;
		if (!store)
			throw "Provider store is missing"; // eslint-disable-line no-throw-literal

		this.setState({ store });

		store.store$.subscribe(data => {
			this.setState({
				data,
			});
		});
	}

	render() {
		const { store } = this.state;
		if (!store)
			return null;

		return (
			<StoreContext.Provider value={this.state}>
				{this.props.children}
			</StoreContext.Provider>
		);
	}
}

const EMPTY_OBJ = {};

const wrapActions = (actions, action$) => {
	if (!actions) return EMPTY_OBJ;
	return Object.keys(actions).reduce((acc, cur) => ({
		...acc,
		[cur]: (...args) => action$.next(actions[cur].apply(null, args))
	}), {});
};

export const withStateContext = (mapStateToProps = null, mapActionsToProps = null) => componentType => {
	const Stateful = () => (
		<StoreContext.Consumer>
			{({store}) => createElement(componentType, {
				...(mapStateToProps && mapStateToProps(store.getState())) || EMPTY_OBJ,
				...wrapActions(mapActionsToProps, store.action$)
			})}
		</StoreContext.Consumer>
	);
	return Stateful;
}
