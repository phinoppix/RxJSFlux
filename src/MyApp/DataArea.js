import React, { Component, Fragment } from "react";

export class DataArea extends Component {
	componentDidMount() {
		const { syncPushData } = this.props;
		syncPushData(20);
	}

	render() {
		const { someList: { counter } } = this.props;
		return (
			<Fragment>
				<div>Counter = {counter}</div>
			</Fragment>
		);
	}
}