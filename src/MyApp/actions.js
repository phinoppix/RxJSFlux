
export const getSomeData = () => ({
	type: "GET_SOME_DATA"
});

export const someDataReceived = (data) => ({
	type: "SOME_DATA_RECEIVED",
	payload: {
		data
	}
});

export const syncPushData = (counter) => ({
	type: "SYNC_PUSH_DATA",
	payload: {
		counter
	}
});
