import React from "react";
import { ConnectedDataArea } from "./ConnectedDataArea";

export const Workspace = () => (
	<ConnectedDataArea />
);